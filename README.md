# Todo server

This application is an active experiment in building a GraphQL server from
scratch and embedding a React application into it to produce a single-binary web
application.

**This is work in progress, and not nearly complete.**

## Features
- React frontend with Apollo GraphQL Client
- Custom GraphQL server on the backend, using
	[uhn/ggql](https://github.com/uhn/ggql) as the GraphQL resolver library.
- Authenticated persistent login sessions using OpenID Connect
- In-memory data store
- Ability to create and mutate todo items.

## Upcoming
- Persistence to a database
- GraphQL subscriptions over websockets
- API responses scoped to the user who initiated them (only see your todos)
- Better CSS styling.


### Running the app
- Look at docker-compose and create a `.env` file with secrets for postgres and
	keycloak root user.
- `docker-compose up -d`
- Login to keycloak and configure an OIDC client with id `todo-react`. Store the
	client secret in the `.env` file under `TODO_CLIENT_SECRET`.
- `cd frontend && npm i && cd ../ && go generate ./...`
- Run `./run.sh` to start the app.
