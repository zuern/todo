package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"sync"
	"time"

	"github.com/nats-io/nuid"
	"gitlab.com/zuern/todo-react/model"
	"golang.org/x/oauth2"
)

// reference materials: https://developers.google.com/identity/protocols/oauth2/openid-connect

type Auth struct {
	config      *oauth2.Config
	userinfoURL string
	store       Store

	states       map[string]int64
	stateCleanAt int64
	lock         sync.Mutex
}

// NewAuth creates a new OIDC auth client using store for persistence and
// discovering openid server configuration from the issuerURL.
func NewAuth(
	ctx context.Context,
	store Store,
	issuerURL string, clientID, clientSecret string,
) (*Auth, error) {
	if issuerURL == "" {
		return nil, fmt.Errorf("oidc issuer url cannot be empty")
	} else if clientID == "" {
		return nil, fmt.Errorf("oidc client id cannot be empty")
	}
	a := &Auth{
		store: store,
		config: &oauth2.Config{
			RedirectURL:  "http://localhost:3000/callback",
			ClientID:     clientID,
			ClientSecret: clientSecret,
			Scopes:       []string{"openid", "email"},
		},
		states: map[string]int64{},
	}
	err := a.discoverConfiguration(ctx, issuerURL)
	if err != nil {
		a = nil
	}
	return a, err
}

func (a *Auth) discoverConfiguration(ctx context.Context, issuerURL string) (err error) {
	if n := len(issuerURL); n > 0 && issuerURL[n-1] == '/' {
		issuerURL = issuerURL[:n-1]
	}
	issuerURL += "/.well-known/openid-configuration"
	type OpenIDConfiguration struct {
		AuthURL     string `json:"authorization_endpoint"`
		TokenURL    string `json:"token_endpoint"`
		UserInfoURL string `json:"userinfo_endpoint"`
	}
	defer func() {
		if err != nil {
			err = fmt.Errorf("oidc configuration discovery failed: %w", err)
		}
	}()
	req, err := http.NewRequestWithContext(ctx, "GET", issuerURL, nil)
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("discovery endpoint returned %d", resp.StatusCode)
	}
	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var discovered OpenIDConfiguration
	if err = json.Unmarshal(bytes, &discovered); err != nil {
		return err
	}
	a.config.Endpoint.AuthURL = discovered.AuthURL
	a.config.Endpoint.TokenURL = discovered.TokenURL
	a.userinfoURL = discovered.UserInfoURL
	return err
}

func (a *Auth) GetSession(req *http.Request) string {
	var sessionID string
	if cookie, _ := req.Cookie("session"); cookie != nil {
		sessionID = cookie.Value
	}
	return sessionID
}

func (a *Auth) HandleLoginRedirect(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	state := nuid.Next()
	loginURI := a.config.AuthCodeURL(state)
	http.Redirect(w, r, loginURI, http.StatusTemporaryRedirect)

	now := time.Now()
	unix := now.UnixNano()

	// store state token in cache, remove any expired entries.
	a.lock.Lock()
	a.states[state] = unix
	if a.stateCleanAt <= unix {
		for k, expiry := range a.states {
			if expiry <= a.stateCleanAt {
				delete(a.states, k)
			}
		}
		const stateExpiration = time.Hour
		a.stateCleanAt = now.Add(stateExpiration).UnixNano()
	}
	a.lock.Unlock()
}

func (a *Auth) HandleCallback(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	state, code := r.FormValue("state"), r.FormValue("code")

	// Check state token to prevent CSRF.
	a.lock.Lock()
	if _, hasState := a.states[state]; hasState {
		delete(a.states, state)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		a.lock.Unlock()
		return
	}
	a.lock.Unlock()

	ctx := r.Context()
	token, err := a.config.Exchange(ctx, code)
	if err != nil {
		return
	}
	// TODO pull out ID token, no need to verify since speaking directly to OP

	// lookup user info
	req, err := http.NewRequestWithContext(ctx, "GET", a.userinfoURL, nil)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token.AccessToken))
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	defer resp.Body.Close()
	contents, err := io.ReadAll(resp.Body)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	type UserInfo struct {
		Sub    string `json:"sub"`
		Email  string `json:"email"`
		Given  string `json:"given_name"`
		Family string `json:"family_name"`
	}
	var info UserInfo
	if err = json.Unmarshal(contents, &info); err != nil {
		w.WriteHeader(500)
		return
	}
	user := &model.Profile{
		ID:     info.Sub,
		Email:  info.Email,
		Given:  info.Given,
		Family: info.Family,
	}
	if err = a.store.AddUser(user); err != nil {
		w.WriteHeader(500)
		return
	}
	var sessionID string
	if sessionID, err = a.store.CreateSession(info.Sub); err != nil {
		w.WriteHeader(500)
		return
	}
	a.setSessionCookie(w, sessionID, 5*time.Minute)
	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}

func (a *Auth) HandleLogout(w http.ResponseWriter, r *http.Request) {
	if session := a.GetSession(r); session != "" {
		a.setSessionCookie(w, "", -1) // unset the session cookie.
		userID, err := a.store.GetSession(session)
		if err == nil {
			err = a.store.DeleteSessions(userID)
		}
		if err != nil {
			w.WriteHeader(500)
		}
	}
	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}

func (a *Auth) setSessionCookie(resp http.ResponseWriter, sessionID string, expires time.Duration) {
	http.SetCookie(resp, &http.Cookie{
		Name:     "session",
		Value:    sessionID,
		Secure:   true,
		HttpOnly: true,
		MaxAge:   int(expires.Seconds()),
	})
}
