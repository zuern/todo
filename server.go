package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/uhn/ggql/pkg/ggql"
)

type Server struct {
	srv    *http.Server
	router *mux.Router
	Auth   *Auth
	Addr   string
	Root   *ggql.Root
	Store  *MemStore
}

func (s *Server) ListenAndServe() error {
	// Set up Server.
	if err := s.registerRoutes(); err != nil {
		return err
	}
	s.srv = &http.Server{
		Handler:      s.router,
		Addr:         ":3000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	return s.srv.ListenAndServe()
}

func (s *Server) registerRoutes() error {
	s.router = mux.NewRouter()
	s.router.Path("/login").HandlerFunc(s.Auth.HandleLoginRedirect)
	s.router.Path("/logout").HandlerFunc(s.Auth.HandleLogout)
	s.router.Path("/callback").HandlerFunc(s.Auth.HandleCallback)
	s.router.HandleFunc("/graphql", s.handleGraphQL).Methods("GET", "POST", "OPTIONS")
	publicFS, err := fs.Sub(frontend, "frontend/build")
	if err == nil {
		s.router.PathPrefix("/").Handler(http.FileServer(http.FS(publicFS)))
	}
	return err
}

func (s *Server) handleGraphQL(res http.ResponseWriter, req *http.Request) {
	// TODO handle CORS in a safer way.
	origin := "*"
	if o := req.Header.Get("Origin"); len(o) > 0 {
		origin = o
	}
	res.Header().Add("Access-Control-Allow-Origin", origin)
	res.Header().Add("Access-Control-Allow-Headers", "*")
	if req.Method == "OPTIONS" {
		return
	}
	body, err := io.ReadAll(req.Body)
	defer func() { _ = req.Body.Close() }()
	if err != nil {
		res.WriteHeader(500)
		return
	}
	var operationName, query string
	var vars map[string]interface{}
	switch req.Header.Get("Content-Type") {
	case "application/json":
		var m map[string]interface{}
		if err = json.Unmarshal(body, &m); err != nil {
			res.WriteHeader(400)
			return
		}
		operationName, _ = m["operationName"].(string)
		query, _ = m["query"].(string)
		vars, _ = m["variables"].(map[string]interface{})
	case "application/graphql":
		query = string(body)
	}
	if q := req.URL.Query().Get("query"); len(q) > 0 {
		query = q
	}
	if op := req.URL.Query().Get("operationName"); len(op) > 0 {
		operationName = op
	}
	if req.Method == "GET" {
		if v := req.URL.Query().Get("variables"); len(v) > 0 {
			if err = json.Unmarshal([]byte(v), &vars); err != nil {
				res.WriteHeader(400)
				gqlResp(nil, err, res)
				return
			}
		}
	}
	exe, err := s.Root.ParseExecutableString(query)
	if err != nil {
		gqlResp(nil, err, res)
		return
	}

	// TODO build some kind of session/user middleware?
	sessionID := s.Auth.GetSession(req)
	var userID string
	if userID, err = s.Store.GetSession(sessionID); err != nil {
		fmt.Println(err)
		res.WriteHeader(500)
		return
	}
	appCtx := &Context{Store: s.Store}
	if appCtx.User, err = s.Store.GetUser(userID); err != nil {
		fmt.Println(err)
		res.WriteHeader(500)
		return
	}
	ctx := NewContext(req.Context(), appCtx)
	exe.SetContextRecursive(ctx)

	data, err := s.Root.ResolveExecutable(exe, operationName, vars)
	gqlResp(data, err, res)
}

func gqlResp(data interface{}, err error, w http.ResponseWriter) {
	resp := &GraphQLResponse{}
	if dm, _ := data.(map[string]interface{}); dm != nil {
		resp.Data = dm["data"]
		if err != nil {
			resp.Errors = ggql.FormErrorsResult(err)
		}
	}
	bytes, _ := json.MarshalIndent(resp, "", "  ")
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(bytes)
}
