package main

import (
	"context"
	"embed"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/uhn/ggql/pkg/ggql"
)

//go:generate sh -c "cd frontend && npm run build"

var (
	//go:embed data.json
	data []byte
	//go:embed schema.graphql
	sdl []byte
	//go:embed frontend/build
	frontend embed.FS
)

func main() {

	var (
		port         = 3000
		issuerURL    = os.Getenv("TODO_OIDC_ISSUER_URL")
		clientID     = os.Getenv("TODO_OIDC_CLIENT_ID")
		clientSecret = os.Getenv("TODO_OIDC_CLIENT_SECRET")
	)

	flag.IntVar(&port, "p", port, "HTTP port")
	flag.StringVar(&clientID, "clientID", clientID, "OIDC ClientID")
	flag.StringVar(&issuerURL, "issuerURL", issuerURL, "OIDC Issuer URL")
	flag.Parse()
	store := &MemStore{}
	_ = json.Unmarshal(data, &store.Todos) // initial todo data to populate store.
	store.id = len(store.Todos)
	// Set up GraphQL
	var root *ggql.Root
	schema := &Schema{}
	root = ggql.NewRoot(schema)
	if err := root.Parse(sdl); err != nil {
		log.Fatalf("parse schema: %s\n", err)
	}
	// Set up Server.
	if clientSecret == "" {
		log.Fatalf("bad env: TODO_OIDC_CLIENT_SECRET not set in environment")
	}
	cx, cf := context.WithTimeout(context.Background(), 5*time.Second)
	auth, err := NewAuth(cx, store, issuerURL, clientID, clientSecret)
	cf()
	if err != nil {
		log.Fatal(err)
	}
	srv := &Server{
		Auth:  auth,
		Addr:  fmt.Sprintf(":%d\n", port),
		Root:  root,
		Store: store,
	}
	fmt.Printf("Serving on %s\n", srv.Addr)
	log.Fatal(srv.ListenAndServe())
}
