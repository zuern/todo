package main

import (
	"context"

	"gitlab.com/zuern/todo-react/model"
)

type Context struct {
	User  *model.Profile
	Store Store
}

type _appCtx struct{}

func NewContext(ctx context.Context, c *Context) context.Context {
	return context.WithValue(ctx, _appCtx{}, c)
}

func FromContext(ctx context.Context) *Context {
	return ctx.Value(_appCtx{}).(*Context)
}
