package model

import "github.com/mitchellh/mapstructure"

type Profile struct {
	ID     string
	Email  string
	Given  string
	Family string
}

type Todo struct {
	ID          string
	Title       string
	Description string
	Done        bool
}

func NewTodo(data map[string]interface{}) (*Todo, error) {
	var todo Todo
	err := mapstructure.Decode(data, &todo)
	return &todo, err
}
