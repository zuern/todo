package main

import (
	"fmt"
	"strconv"

	"github.com/nats-io/nuid"
	"gitlab.com/zuern/todo-react/model"
)

type MemStore struct {
	Todos []*model.Todo
	id    int

	Users          map[string]*model.Profile
	Sessions       map[string]string
	userToSessions map[string][]string
}

/*
	Deliberately naive implementation, minimal error handling, can panic with
	bad input. Just something simple to power the API.
*/

func (s *MemStore) GetTodo(id string) (*model.Todo, error) {
	var todo *model.Todo
	for _, t := range s.Todos {
		if t.ID == id {
			todo = t
			break
		}
	}
	return todo, nil
}

func (s *MemStore) ListTodos() ([]*model.Todo, error) {
	return s.Todos, nil
}

func (s *MemStore) AddTodo(t *model.Todo) *model.Todo {
	t.ID = strconv.Itoa(s.id)
	s.id++
	s.Todos = append(s.Todos, t)
	return t
}

func (s *MemStore) UpdateTodo(id string, t *model.Todo) (todo *model.Todo, err error) {
	if todo, err = s.GetTodo(id); err != nil {
		return
	}
	if todo == nil {
		err = fmt.Errorf("todo not found")
		return
	}
	*todo = model.Todo{
		ID:          id,
		Title:       t.Title,
		Description: t.Description,
		Done:        t.Done,
	}
	return todo, err
}

func (s *MemStore) DeleteTodo(id string) error {
	index := -1
	for i, todo := range s.Todos {
		if todo.ID == id {
			index = i
			break
		}
	}
	if index >= 0 {
		if index == 0 && len(s.Todos) == 1 {
			s.Todos = s.Todos[:0]
		} else {
			s.Todos = append(s.Todos[:index], s.Todos[index+1:]...)
		}
	}
	return nil
}

func (m *MemStore) AddUser(user *model.Profile) error {
	if m.Users == nil {
		m.Users = map[string]*model.Profile{user.ID: user}
	} else {
		m.Users[user.ID] = user
	}
	return nil
}

func (m *MemStore) GetUser(id string) (*model.Profile, error) {
	var user *model.Profile
	if m.Users != nil {
		user = m.Users[id]
	}
	return user, nil
}

func (m *MemStore) GetSession(id string) (userEmail string, err error) {
	if m.Sessions != nil {
		userEmail = m.Sessions[id]
	}
	return
}

func (m *MemStore) CreateSession(userID string) (sessionID string, err error) {
	sessionID = nuid.Next()
	if m.Sessions == nil {
		m.Sessions = map[string]string{sessionID: userID}
		m.userToSessions = map[string][]string{sessionID: []string{userID}}
	} else {
		m.Sessions[sessionID] = userID
		m.userToSessions[userID] = append(m.userToSessions[userID], sessionID)
	}
	return
}

func (m *MemStore) DeleteSessions(userID string) error {
	if m.Sessions != nil {
		for _, session := range m.userToSessions[userID] {
			delete(m.Sessions, session)
		}
		delete(m.userToSessions, userID)
	}
	return nil
}
