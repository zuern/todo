module gitlab.com/zuern/todo-react

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/mapstructure v1.4.1
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/uhn/ggql v1.2.11
	golang.org/x/oauth2 v0.0.0-20210220000619-9bb904979d93
)
