import './App.css';
import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
} from '@apollo/client';

import TodoList from './components/TodoList';

const client = new ApolloClient({
  uri: 'http://localhost:3000/graphql',
  cache: new InMemoryCache()
});


function App() {
  return (
    <ApolloProvider client={client}>
    <div className="container max-w-4xl mx-auto">
      <TodoList/>
    </div>
    </ApolloProvider>
  );
}

export default App;
