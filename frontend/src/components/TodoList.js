import { useQuery } from '@apollo/client';
import { GET_TODOLIST } from '../graphql.js';
import Todo from './Todo.js';
import AddTodo from './AddTodo';

export default function TodoList() {
  const { loading, error, data } = useQuery(GET_TODOLIST);
  let content;
  if (loading) {
    content = <p>Loading...</p>
  } else if (error) {
    content = `<p>Error: ${error.message}</p>`
  } else {
    const todos = data.todos.map((todo) => <Todo todo={todo} key={todo.id} />);
    let profileInfo = (<a href="/login">Click here to log in</a>)
    if (data.user) {
      profileInfo = (
        <div>
          <h3>Hi {data.user.given} {data.user.family}!</h3>
          <p>Your email is: {data.user.email}.</p>
          <a href="/logout">Click here to log out</a>
        </div>
      )
    }
    content = (
      <div>
        {profileInfo}
        <ul>
          {todos}
          <li className="ml-11"><AddTodo/></li>
        </ul>
      </div>
    )
  }
  return (
  <div>
    <h1 className="pt-2 pb-0">To Do:</h1>
    {content}
  </div>
  );
}
