import { useMutation } from '@apollo/client';
import { UPDATE_TODO } from '../graphql.js';

export default function Todo(state) {
  const [ updateTodo ] = useMutation(UPDATE_TODO);
  const todo = state.todo;
  // copy todo, executes a func to modify the todo and sends a GraphQL mutation
  // with the data.
  const update = (func) => {
    let updated = { ...todo };
    updated = func(updated);
    updated.done = !todo.done;
    delete updated.__typename;
    delete updated.id; // TODO find out why without this the mutation breaks.
    updateTodo({variables: { id: todo.id, todo: updated}});
  };

  return (
    <li className="border-solid bordert-2 flex items-start my-2">
      <button
        width="16"
        height="16"
        className="mx-2 mt-1.5 rounded-full border-2 border-gray-400 hover:bg-gray-200 transition flex items-center justify-center focus:outline-none"
        onClick={(e) => {
          e.preventDefault();
          update((update) => {
            update.done = !todo.done;
            return update;
          });
        }}>
        <svg width="24" height="24"><path fill={todo.done ? "fill-current" : "#fff"} d="M11.23 13.7l-2.15-2a.55.55 0 0 0-.74-.01l.03-.03a.46.46 0 0 0 0 .68L11.24 15l5.4-5.01a.45.45 0 0 0 0-.68l.02.03a.55.55 0 0 0-.73 0l-4.7 4.35z"></path></svg>
      </button>
      <div className="mt-1">
        <h3 className="inline-block">{todo.title}</h3>
        {todo.description && (
        <p className="whitespace-pre-line text-sm">{todo.description}</p>
        )}
      </div>
    </li>
  )
}
