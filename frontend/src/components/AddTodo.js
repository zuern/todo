import { useState } from 'react';
import { useMutation } from '@apollo/client';
import { GET_TODOS, ADD_TODO } from '../graphql.js';

export default function AddTodo() {
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");

  const updateCache = (cache, {data}) => {
    const existingTodos = cache.readQuery({query: GET_TODOS});
    const newTodo = data.addTodo;
    cache.writeQuery({
      query: GET_TODOS,
      data: {todos: [newTodo, ...existingTodos.todos]},
    });
  };

  const [addTodo] = useMutation(ADD_TODO, {update: updateCache});
  const handleSubmit = (e) => {
    e.preventDefault();
    const newTodo = {
      title: title,
      description: desc,
    }
    console.log(newTodo);
    addTodo({variables: {todo: newTodo }});
    setTitle("");
    setDesc("");
  };
  return (
    <form onSubmit={handleSubmit}>
      <input placeholder="Title..." type="text" name="title" onChange={e => (setTitle(e.target.value))} />
      <textarea placeholder="Description..." name="title" onChange={e => (setDesc(e.target.value))} />
      <input type="submit" value="Add" />
    </form>
  )
};
