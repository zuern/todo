import { gql } from '@apollo/client';

export const TODO_FIELDS_FRAG = gql`
  fragment TodoFieldsFragment on Todo {
    id
    title
    description
    done
  }
`

export const GET_TODOS = gql`
  {
    todos {
      ...TodoFieldsFragment
    }
  }
  ${TODO_FIELDS_FRAG}
`

export const GET_TODOLIST = gql`
  {
    todos {
      ...TodoFieldsFragment
    }
    user {
      email
      family
      given
    }
  }
  ${TODO_FIELDS_FRAG}
`;

export const ADD_TODO = gql`
  mutation addTodo($todo: TodoInput!) {
    addTodo(todo: $todo) {
      ...TodoFieldsFragment
    }
  }
  ${TODO_FIELDS_FRAG}
`

export const UPDATE_TODO = gql`
  mutation updateTodo($id: ID!, $todo: TodoInput!) {
    updateTodo(id: $id, todo: $todo) {
      ...TodoFieldsFragment
    }
  }
  ${TODO_FIELDS_FRAG}
`
