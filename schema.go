package main

import (
	"context"
	"fmt"

	"github.com/uhn/ggql/pkg/ggql"
	"gitlab.com/zuern/todo-react/model"
)

type Schema struct {
	Query    Query
	Mutation Mutation
}

type Query struct{}

func (q Query) Resolve(field *ggql.Field, _ map[string]interface{}) (interface{}, error) {
	ctx := FromContext(field.Context.(context.Context))
	switch field.Name {
	case "todos":
		return ctx.Store.ListTodos()
	case "user":
		return ctx.User, nil
	}
	return nil, fmt.Errorf("unknown field")
}

type Mutation struct{}

func (m Mutation) Resolve(field *ggql.Field, args map[string]interface{}) (interface{}, error) {
	store := FromContext(field.Context.(context.Context)).Store
	switch field.Name {
	case "addTodo":
		todo, err := model.NewTodo(args["todo"].(map[string]interface{}))
		if err != nil {
			return nil, err
		}
		return store.AddTodo(todo), nil
	case "updateTodo":
		id := args["id"].(string)
		todo, err := model.NewTodo(args["todo"].(map[string]interface{}))
		if err != nil {
			return nil, err
		}
		return store.UpdateTodo(id, todo)
	case "deleteTodo":
		id := args["id"].(string)
		err := store.DeleteTodo(id)
		return err == nil, err
	}
	return nil, fmt.Errorf("unknown field")
}
