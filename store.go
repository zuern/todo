package main

import (
	"gitlab.com/zuern/todo-react/model"
)

type Store interface {
	GetTodo(id string) (*model.Todo, error)
	ListTodos() ([]*model.Todo, error)
	AddTodo(t *model.Todo) *model.Todo
	UpdateTodo(id string, t *model.Todo) (*model.Todo, error)
	DeleteTodo(id string) error

	AddUser(user *model.Profile) error
	GetUser(id string) (*model.Profile, error)

	GetSession(id string) (userEmail string, err error)
	CreateSession(userID string) (sessionID string, err error)
	DeleteSessions(userID string) error
}
