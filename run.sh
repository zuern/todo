#!/bin/sh

set -e

cd "$(dirname $0)"

if [ ! -f .env ]; then
	echo "Missing .env file containing secrets to start local Keycloak and Postgres. See docker-compose.yml"
	exit 1
fi

docker-compose up -d
export $(cat .env | xargs) && go run *.go

